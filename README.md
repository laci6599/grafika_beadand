# Nagy László - Számítógépi grafika beadandó

 **Feladatleírás:** Feladatomban egy régimódi szellemházat szimulálok, amelyben található egy forgatható Ferrari is.

# Funkciók:

- Kamerakezelés
- Tér körbejárása
- 3D objektumok betöltése
- Textúrák
- Fényerő növelés, csökkentés
- Mozgás (Forgatható ferrari objektum)
- Használati útmutató a programban


## Használati útmutató

- Kamera forgatás
	Bal klikk + egér mozgatás
	
- Mozgatás
	W A S D billentyűk
	
- Objektum (ferrari) forgatása
	F billentyű megnyomása a ki/be kapcsoláshoz
	
- Fényerő növelés/csökkentés
	"+", "-" billentyűk
	
- HELP menü
	F1 billentyű
	
## Indítás: 

A mappa tartalmazza az assets mappát így azt külön betölteni nem kell, egy Make parancs kiadása után a house.exe fájl megnyitásával már meg is 
tekinthető meg a program.


