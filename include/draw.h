#include "model.h"

#ifndef INCLUDE_DRAW_H_
#define INCLUDE_DRAW_H_

#ifndef GL_EXT_texture_edge_clamp
#define GL_CLAMP_TO_EDGE                 0x812F
#endif


typedef struct Position
{
	double x;
	double y;
	double z;
	
} Position;


typedef struct
{
	double ferrari_rotation;
	
} Rotate;


void draw_objects(World* world,Rotate* rotate);


void draw_ground(Grass grass);


void draw_walls(Grass grass);


void draw_model(const struct Model* model);


void draw_help(int texture);

#endif 
