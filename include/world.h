#ifndef WORLD_H_
#define WORLD_H_
#include <stdio.h>
#include "object.h"


typedef struct {
	
    int front, back, left, right, ground, top;
	
} Grass;


typedef struct {
	
	Grass grass;
	Object house;
	Object ferrari;
	Object wood;
	int helpTexture;
	
} World;

World world;

#endif